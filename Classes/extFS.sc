+ FSReq {

	getSync { |objClass|
		cmd.systemCmd;
		^objClass.new(
			File(filePath,"r").readAllString.postln;
			Freesound.parseFunc.value(
				File(filePath,"r").readAllString
			)
		)
	}

	*retrieveSync { |uri, path|
		"curl -H '%' '%'>'%'".format(FSReq.getHeader, uri, path).systemCmd
	}


}

+ Freesound {

	*getTokenSync { |code, refresh = false|
		var tmpFile = PathName.tmp ++ "fs_" ++ UniqueID.next ++ ".txt";
		var tokenObj;
		var cmd =  "curl -X POST -d 'client_id=%&client_secret=%";
		cmd = format(cmd, Freesound.clientId, Freesound.clientSecret);
		if(refresh){
			cmd = cmd ++ "&grant_type=refresh_token&refresh_token=%"
		}{
			cmd = cmd ++ "&grant_type=authorization_code&code=%"
		};
		cmd = format(cmd, code);
		cmd = cmd ++ "' https://www.freesound.org/apiv2/oauth2/access_token/ >'%'";
		cmd = format(cmd, tmpFile);
		cmd.systemCmd;

		tokenObj = parseFunc.value(File(tmpFile,"r").readAllString);
		tokenObj.postln;
		if(tokenObj.includesKey("access_token")){
			Freesound.token = tokenObj["access_token"];
			File.delete(tokenFile);
			File.copy(tmpFile, tokenFile);
		}
	}

	*refreshTokenSync{
		var tokenObj = parseFunc.value(File(tokenFile, "r").readAllString);
		Freesound.getTokenSync(tokenObj["refresh_token"], refresh: true);
	}
}