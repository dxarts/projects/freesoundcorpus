FreesoundCorpus {
	var <>clientId, <>clientSecret;

	*new { |clientId, clientSecret|
		^super.newCopyArgs(clientId, clientSecret).init
	}

	init {
		Freesound.clientId = clientId;
		Freesound.clientSecret = clientSecret;
		Freesound.authType = "oauth2"
	}

	// authorize and get token
	getAuth { Freesound.getAuthorizationPage }
	getToken { |tokenCode| Freesound.getTokenSync(tokenCode) }
	refreshToken { Freesound.refreshTokenSync }

	download { |fssounds, folder|
		fssounds = fssounds.asArray;
		fssounds.do{ |fssound|
			FSReq.retrieveSync(Freesound.uri(\DOWNLOAD, fssound.id), folder +/+ fssound.name.replace("/").replaceExtension(fssound.type))
		}
	}

	clumpPages { |numSounds| ^(numSounds/150).ceil.asInteger.collect{ |i| (numSounds > 150).if({ numSounds = numSounds - 150; 150 }, { numSounds }) } }

	search { |type = \TEXT_SEARCH, params| ^FSReq.new(Freesound.uri(type), params).getSync(FSPager).results.collect{ |result| FSSound.new(result) } }

	pageSearch { |type, filter, numSounds, extraParams|
		var params;
		^this.clumpPages(numSounds).collect{ |num, page|
			params = ('page': page+1, 'page_size': 150, 'fields': "id,name,type");
			params.putAll(extraParams);
			params.putAll(('filter': "type:(wav OR aiff)" + filter));
			this.search(type, params)[0..num-1]
		}.flatten
	}

	textSearch { |query, filter, sort, numSounds|
		^this.pageSearch(\TEXT_SEARCH, filter, numSounds, ('query': query, 'sort': sort))
	}

	contentSearch { |target, numSounds|
		^this.pageSearch(\CONTENT_SEARCH, nil, numSounds, ('target': target))
	}

	combinedSearch { |query, target, filter, descriptorsFilter, sort, numSounds|
		^this.pageSearch(\COMBINED_SEARCH, filter, numSounds, ('query': query,
			'sort': sort, 'target': target, 'descriptors_filter': descriptorsFilter))
	}

}